﻿using UnityEngine;

public class Cell : MonoBehaviour
{
    public bool isEmptyCell { get; set; }
    public int CellValue { get; set; }

    public int RowPosition { get; set; }
    public int ColumnPosition { get; set; }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Board
{
    /// <summary>
    /// Create cell and values for them
    /// </summary>
    public class Board : MonoBehaviour
    {
        private const int CellCount = 15;

        private const int Rows = 4;
        private const int Columns = 4;

        private GameObject _cellResource;

        private List<int> randomValues = new List<int>();

        void Start()
        {
            _cellResource = Resources.Load("Cell") as GameObject;

            GenerateRandomValues();
            CreateCells();
        }

        /// <summary>
        /// Generate random cells value
        /// </summary>
        private void GenerateRandomValues()
        {
            for (int i = 1; i < CellCount + 1; i++)
            {
                int randomNumber = Random.Range(1, CellCount + 1);

                while (randomValues.Contains(randomNumber))
                {
                    randomNumber = Random.Range(1, CellCount + 1);
                }

                randomValues.Add(randomNumber);
            }
        }

        /// <summary>
        /// Create cells and 1 empty cell
        /// </summary>
        private void CreateCells()
        {
            int currentCell = 0;

            for (int column = 0; column < Columns; column++)
            {
                for (int row = 0; row < Rows; row++)
                {
                    if (LastRowAndColumn(column, row))
                    {
                        var createdEmptyCell = Instantiate(_cellResource, transform);

                        createdEmptyCell.name = "EmptyCell";

                        var emptyCell = createdEmptyCell.GetComponent<Cell>();

                        emptyCell.ColumnPosition = column;
                        emptyCell.RowPosition = row;
                        emptyCell.isEmptyCell = true;
                         
                        break;
                    }

                    var createdCell = Instantiate(_cellResource, transform);

                    var valueText = createdCell.GetComponentInChildren<Text>();
                    var cell = createdCell.GetComponent<Cell>();

                    createdCell.name = "Cell " + randomValues[currentCell];

                    valueText.text = randomValues[currentCell].ToString();
                    cell.CellValue = randomValues[currentCell];
                    cell.RowPosition = row;
                    cell.ColumnPosition = column;

                    currentCell++;
                }
            }


        }

        private bool LastRowAndColumn(int column, int row)
        {
            return column >= Columns - 1 && row >= Rows - 1;
        }
    }
}

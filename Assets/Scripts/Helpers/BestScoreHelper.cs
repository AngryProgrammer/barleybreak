﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Save best score data
/// </summary>
public class BestScoreHelper : MonoBehaviour
{
    private void Start()
    {
        DontDestroyOnLoad(this);

        var bestScoreText = GameObject.Find("BestScoreText").GetComponent<Text>();

        bestScoreText.text = "Best score - " + BestScore;
    }

    public int BestScore
    {
        get
        {
            if (!PlayerPrefs.HasKey("BestScore"))
            {
                PlayerPrefs.SetInt("BestScore", 0);
            }

            return PlayerPrefs.GetInt("BestScore");
        }
        set
        {
            PlayerPrefs.SetInt("BestScore", value);
        }

    }
}

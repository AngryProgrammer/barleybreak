﻿using Assets.Scripts.Board;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Swap cell and determine victory
/// </summary>
public class GameManager : MonoBehaviour
{
    private int movesCount = 0;

    private Board _board;
    private Text _movesCountText;
    private GraphicRaycaster _raycaster;
    private PointerEventData _pointerEventData;
    private EventSystem _eventSystem;
    private BestScoreHelper _bestScoreHelper;

    void Start()
    {
        _bestScoreHelper = FindObjectOfType<BestScoreHelper>();
        var canvas = GameObject.Find("Canvas");
        _board = FindObjectOfType<Board>();
        _movesCountText = GameObject.Find("MovesCountText").GetComponent<Text>();
        _raycaster = canvas.GetComponent<GraphicRaycaster>();
        _eventSystem = canvas.GetComponent<EventSystem>();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            _pointerEventData = new PointerEventData(_eventSystem)
            {
                position = Input.mousePosition
            };

            var results = new List<RaycastResult>();

            _raycaster.Raycast(_pointerEventData, results);

            foreach (var result in results)
            {
                CellClick(result);
            }
        }
    }

    /// <summary>
    /// Click on cell
    /// </summary>
    /// <param name="result"></param>
    private void CellClick(RaycastResult result)
    {
        var cell = result.gameObject.GetComponent<Cell>();

        if (cell != null)
        {
            var emptyCell = FindObjectsOfType<Cell>().Where(x => x.isEmptyCell == true).Single();

            if (NearEmptyCell(cell, emptyCell))
            {
                Swap(cell, emptyCell);

                movesCount++;
                _movesCountText.text = movesCount.ToString();

                if (IsVictory())
                {
                    _bestScoreHelper.BestScore = movesCount;

                    var gameOverPanel = GameObject.Find("GameOver").transform.GetChild(0);

                    gameObject.SetActive(true);
                }
            }
        }
    }

    /// <summary>
    /// Check game on the victory
    /// </summary>
    /// <returns></returns>
    private bool IsVictory()
    {
        for (int cell = 1; cell < _board.transform.childCount - 1; cell++)
        {
            var value = _board.transform.GetChild(cell).GetComponent<Cell>().CellValue;
            var previousValue = _board.transform.GetChild(cell - 1).GetComponent<Cell>().CellValue;

            if(value - 1 != previousValue) 
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Swap empty cell with standart cell
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="emptyCell"></param>
    private void Swap(Cell cell, Cell emptyCell)
    {
        var tempIsEmptyCell = cell.isEmptyCell;
        var tempCellName = cell.name;
        var tempValue = cell.CellValue;
        var tempTextValue = cell.GetComponentInChildren<Text>().text;

        cell.isEmptyCell = emptyCell.isEmptyCell;
        cell.name = emptyCell.name;
        cell.CellValue = emptyCell.CellValue;
        cell.GetComponentInChildren<Text>().text = emptyCell.GetComponentInChildren<Text>().text;

        emptyCell.name = tempCellName;
        emptyCell.isEmptyCell = tempIsEmptyCell;
        emptyCell.CellValue = tempValue;
        emptyCell.GetComponentInChildren<Text>().text = tempTextValue;
    }

    /// <summary>
    /// Checks if an empty cell is nearby  
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="emptyCell"></param>
    /// <returns></returns>
    private bool NearEmptyCell(Cell cell, Cell emptyCell)
    {
        return IsRightSide(cell, emptyCell) ||
            IsLeftSide(cell, emptyCell) ||
            IsAbove(cell, emptyCell) ||
            IsBelow(cell, emptyCell);
    }

    /// <summary>
    /// Checks if an empty cell is above  
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="emptyCell"></param>
    /// <returns></returns>
    private bool IsAbove(Cell cell, Cell emptyCell)
    {
        return cell.ColumnPosition == emptyCell.ColumnPosition + 1 && cell.RowPosition == emptyCell.RowPosition;
    }

    /// <summary>
    /// Checks if an empty cell is below  
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="emptyCell"></param>
    /// <returns></returns>
    private bool IsBelow(Cell cell, Cell emptyCell)
    {
        return cell.ColumnPosition == emptyCell.ColumnPosition - 1 && cell.RowPosition == emptyCell.RowPosition;
    }

    /// <summary>
    /// Checks if an empty cell is right side  
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="emptyCell"></param>
    /// <returns></returns>
    private bool IsRightSide(Cell cell, Cell emptyCell)
    {
        return cell.ColumnPosition == emptyCell.ColumnPosition && cell.RowPosition + 1 == emptyCell.RowPosition;
    }

    /// <summary>
    /// Checks if an empty cell is left side  
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="emptyCell"></param>
    /// <returns></returns>
    private bool IsLeftSide(Cell cell, Cell emptyCell)
    {
        return cell.ColumnPosition == emptyCell.ColumnPosition && cell.RowPosition - 1 == emptyCell.RowPosition;
    }
}